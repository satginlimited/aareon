﻿using AareonTechnicalTest.Models;
using System.Threading.Tasks;

namespace AareonTechnicalTest.RepositoryAbstractions
{
    public interface ITicketRepository
    {
        Task<int> CreateTicketAsync(Ticket ticket);
    }
}
