﻿using AareonTechnicalTest.Models;
using System.Threading.Tasks;

namespace AareonTechnicalTest.RepositoryAbstractions
{
    public interface INoteRepository
    {
        Task<int> CreateNoteAsync(Note note);
        Task DeleteNoteAsync(int personId, int ticketId, int noteId);
    }
}
