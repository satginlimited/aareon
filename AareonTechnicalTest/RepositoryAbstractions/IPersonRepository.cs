﻿using AareonTechnicalTest.Models;
using System.Threading.Tasks;

namespace AareonTechnicalTest.RepositoryAbstractions
{
    public interface IPersonRepository
    {
        Task<Person> GetPersonByIdAsync(int personId);
    }
}
