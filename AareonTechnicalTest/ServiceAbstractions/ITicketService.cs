﻿using AareonTechnicalTest.DTOs;
using System.Threading.Tasks;

namespace AareonTechnicalTest.ServiceAbstractions
{
    public interface ITicketService
    {
        Task<int> CreateTicketAsync(CreateTicketDto createTicketDto);
    }
}
