﻿using AareonTechnicalTest.DTOs;
using System.Threading.Tasks;

namespace AareonTechnicalTest.ServiceAbstractions
{
    public interface INoteService
    {
        Task<int> CreateNoteAsync(int personId, int ticketId, CreateNoteDto createNoteDto);
        Task<bool> DeleteNoteAsync(int personId, int ticketId, int noteId);
    }
}
