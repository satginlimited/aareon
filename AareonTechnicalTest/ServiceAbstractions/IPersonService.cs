﻿using System.Threading.Tasks;

namespace AareonTechnicalTest.ServiceAbstractions
{
    public interface IPersonService
    {
        Task<bool> IsAdminAsync(int personId);
    }
}
