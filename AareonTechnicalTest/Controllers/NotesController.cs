﻿using AareonTechnicalTest.DTOs;
using AareonTechnicalTest.ServiceAbstractions;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace AareonTechnicalTest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NotesController : ControllerBase
    {
        private readonly INoteService _noteService;

        public NotesController(INoteService noteService)
        {
            _noteService = noteService;
        }

        [HttpPost("{personId:int}/tickets/{ticketId:int}/notes")]
        public async Task<IActionResult> Post([FromRoute] int personId, [FromRoute] int ticketId, [FromBody] CreateNoteDto createNoteDto)
        {
            var noteId = await _noteService.CreateNoteAsync(personId, ticketId, createNoteDto);

            return Ok(noteId);
        }

        [HttpDelete("{personId:int}/tickets/{ticketId:int}/notes/{noteId:int}")]
        public async Task<IActionResult> Delete([FromRoute] int personId, [FromRoute] int ticketId, [FromRoute] int noteId)
        {
            var isAdmin = await _noteService.DeleteNoteAsync(personId, ticketId, noteId);
            if (!isAdmin)
            {
                return Unauthorized();
            }

            return Ok();
        }
    }
}
