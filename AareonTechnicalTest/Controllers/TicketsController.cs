﻿using AareonTechnicalTest.DTOs;
using AareonTechnicalTest.ServiceAbstractions;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace AareonTechnicalTest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TicketsController : ControllerBase
    {
        private readonly ITicketService _ticketService;

        public TicketsController(ITicketService ticketService)
        {
            _ticketService = ticketService;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateTicketDto createTicketDto)
        {
            var ticketId = await _ticketService.CreateTicketAsync(createTicketDto);

            return Ok(ticketId);
        }
    }
}
