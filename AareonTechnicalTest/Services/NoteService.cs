﻿using AareonTechnicalTest.DTOs;
using AareonTechnicalTest.Models;
using AareonTechnicalTest.RepositoryAbstractions;
using AareonTechnicalTest.ServiceAbstractions;
using System.Threading.Tasks;

namespace AareonTechnicalTest.Services
{
    public class NoteService : INoteService
    {
        private readonly INoteRepository _noteRepository;
        private readonly IPersonService _personService;
        public NoteService(
                INoteRepository noteRepository,
                IPersonService personService
            )
        {
            _noteRepository = noteRepository;
            _personService = personService;
        }

        public async Task<int> CreateNoteAsync(int personId, int ticketId, CreateNoteDto createNoteDto)
        {
            var note = new Note
            {
                Content = createNoteDto.Content,
                PersonId = personId,
                TicketId = ticketId
            };

            await _noteRepository.CreateNoteAsync(note);

            return note.Id;
        }

        public async Task<bool> DeleteNoteAsync(int personId, int ticketId, int noteId)
        {
            var isAdmin = await _personService.IsAdminAsync(personId);
            if (isAdmin)
            {
                await _noteRepository.DeleteNoteAsync(personId, ticketId, noteId);                
                return true;
            }

            return false;
        }
    }
}
