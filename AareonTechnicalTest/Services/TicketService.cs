﻿using AareonTechnicalTest.DTOs;
using AareonTechnicalTest.Models;
using AareonTechnicalTest.RepositoryAbstractions;
using AareonTechnicalTest.ServiceAbstractions;
using System.Threading.Tasks;

namespace AareonTechnicalTest.Services
{
    public class TicketService : ITicketService
    {
        private readonly ITicketRepository _ticketRepository;

        public TicketService(ITicketRepository ticketRepository)
        {
            _ticketRepository = ticketRepository;
        }

        public async Task<int> CreateTicketAsync(CreateTicketDto createTicketDto)
        {
            var ticket = new Ticket
            {
                Content = createTicketDto.Content,
                PersonId = createTicketDto.PersonId
            };

            return await _ticketRepository.CreateTicketAsync(ticket);
        }
    }
}
