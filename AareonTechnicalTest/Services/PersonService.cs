﻿using AareonTechnicalTest.ServiceAbstractions;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace AareonTechnicalTest.Services
{
    public class PersonService : IPersonService
    {
        private readonly ApplicationContext _applicationContext;

        public PersonService(ApplicationContext applicationContext)
        {
            _applicationContext = applicationContext;
        }
        public async Task<bool> IsAdminAsync(int personId)
        {
            var person = await _applicationContext.Persons
                .FirstOrDefaultAsync(p => p.Id == personId);

            if (person is object && person.IsAdmin)
            {
                return true;
            }

            return false;
        }
    }
}
