﻿using AareonTechnicalTest.Models;
using AareonTechnicalTest.RepositoryAbstractions;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace AareonTechnicalTest.Repositories
{
    public class NoteRepository : INoteRepository
    {
        private readonly ApplicationContext _applicationContext;

        public NoteRepository(ApplicationContext applicationContext)
        {
            _applicationContext = applicationContext;
        }

        public async Task<int> CreateNoteAsync(Note note)
        {
            _applicationContext.Notes.Add(note);
            await _applicationContext.SaveChangesAsync();

            return note.Id;
        }

        public async Task DeleteNoteAsync(int personId, int ticketId, int noteId)
        {
            var note = await _applicationContext.Notes
                .FirstOrDefaultAsync(x => x.Id == noteId &&
                                          x.PersonId == personId &&
                                          x.TicketId == ticketId);

            if (note is object)
            {
                _applicationContext.Notes.Remove(note);
                await _applicationContext.SaveChangesAsync();
            }
        }
    }
}
