﻿using AareonTechnicalTest.Models;
using AareonTechnicalTest.RepositoryAbstractions;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace AareonTechnicalTest.Repositories
{
    public class PersonRepository : IPersonRepository
    {
        private readonly ApplicationContext _applicationContext;

        public PersonRepository(ApplicationContext applicationContext)
        {
            _applicationContext = applicationContext;
        }

        public async Task<Person> GetPersonByIdAsync(int personId)
        {
            return await _applicationContext.Persons
                .FirstOrDefaultAsync(p => p.Id == personId);
        }
    }
}
