﻿using AareonTechnicalTest.Models;
using AareonTechnicalTest.RepositoryAbstractions;
using System.Threading.Tasks;

namespace AareonTechnicalTest.Repositories
{
    public class TicketRepository : ITicketRepository
    {
        private readonly ApplicationContext _applicationContext;

        public TicketRepository(ApplicationContext applicationContext)
        {
            _applicationContext = applicationContext;
        }

        public async Task<int> CreateTicketAsync(Ticket ticket)
        {
            _applicationContext.Tickets.Add(ticket);
            await _applicationContext.SaveChangesAsync();

            return ticket.Id;
        }
    }
}
