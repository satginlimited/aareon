﻿namespace AareonTechnicalTest.DTOs
{
    public class CreateTicketDto
    {
        public string Content { get; set; }

        public int PersonId { get; set; }
    }
}
